import tempfile
import openpyxl

from django.shortcuts import get_object_or_404
from django.db import transaction
from rest_framework import generics, views, parsers, response, renderers
from django.http import HttpResponse, Http404

from audioservice.models import Track, TrackData, Album, Artist, ForbiddenTrack
from audioservice.serializer import TrackSerializer, ForbiddenTrackSerializer
from audioservice.track_manager import TrackManager


class TrackListView(generics.ListAPIView):
    queryset = Track.objects.all()
    serializer_class = TrackSerializer


class TrackDownloadView(views.APIView):

    def get(self, request, track_id):
        track = get_object_or_404(Track, id=track_id)
        resp = HttpResponse(track.data.content, content_type="application/force-download")
        resp["Content-Disposition"] = f'attachment; filename="{track.name}.{track.format}"'
        resp["Content-type"] = "Content-Type: audio/mpeg"

        # checking if uploaded track has forbidden Artist-Name title
        forbidden_tracks = ForbiddenTrack.objects.filter(name=track.name,
                                                         artist__in=track.artists.values_list('name', flat=True))

        # raise 404 on download attempt
        if forbidden_tracks:
            raise Http404

        return resp


class TrackUploadView(views.APIView):
    parser_classes = [parsers.FileUploadParser]

    def put(self, request):
        data_file = request.data['file']
        track_manager = TrackManager.from_settings()
        parsed = track_manager.parse_file(data_file)

        artist_names = parsed.get("artists", [])
        album_names = parsed.get("albums")
        if album_names:
            album_name = album_names[0]
        else:
            album_name = 'Unknown album'  # preventing UnboundLocalError
        track_names = parsed.get("titles")
        if track_names:
            track_name = track_names[0]
        else:
            track_name = 'Unknown track name'  # preventing UnboundLocalError
        track_duration = parsed["duration"]
        album_release_date = None
        track_format = parsed["format"]

        # checking if uploaded track has forbidden Artist-Name title
        forbidden_tracks = ForbiddenTrack.objects.filter(name=track_name,
                                                         artist__in=artist_names)

        if forbidden_tracks:
            return response.Response({"error": "You cannot upload this song."})

        artists = [
            Artist.objects.get_or_create(name=name)[0]
            for name in artist_names
        ]
        album = track_manager.create_album(album_name, album_release_date, artists)
        track = track_manager.create_track(
            track_name, data_file, track_format, artists=artists,
            duration=track_duration, album=album
        )

        return response.Response(TrackSerializer(track).data)


# list of the forbidden tracks
class ForbiddenTrackListView(generics.ListAPIView):
    queryset = ForbiddenTrack.objects.all()
    serializer_class = ForbiddenTrackSerializer


# here we parsing the XLS file and record its data into database with 'openpyxl' library
class XLSUploadView(views.APIView):
    parser_classes = [parsers.FileUploadParser]

    def put(self, request):
        data_file = request.data['file']
        wb = openpyxl.load_workbook(filename=data_file)

        first_sheet = wb.sheetnames[0]
        worksheet = wb[first_sheet]

        artists = []
        tracks = []

        for i in worksheet['A'][1:]:
            artists.append(i.value)
        for i in worksheet['B'][1:]:
            tracks.append(i.value)

        result = list(zip(artists, tracks))

        for artist, track in result:
            ForbiddenTrack.objects.get_or_create(name=track, artist=artist)

        return response.Response({"success": "XLS file has been succesfully parsed!"}, status=204)
