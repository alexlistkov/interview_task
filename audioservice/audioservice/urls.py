from django.urls import path

from audioservice.views import TrackListView, TrackDownloadView, TrackUploadView, XLSUploadView, ForbiddenTrackListView

urlpatterns = [
    path('tracks/', TrackListView.as_view(), name="track_list"),
    path('forbidden_tracks/', ForbiddenTrackListView.as_view(), name="forbidden_track_list"),  # new url
    path('tracks/upload/', TrackUploadView.as_view(), name="track_upload"),
    path('forbidden_tracks/xls_upload/', XLSUploadView.as_view(), name="xls_upload"),  # new url
    path('tracks/<int:track_id>/', TrackDownloadView.as_view(), name="track_download"),
]
